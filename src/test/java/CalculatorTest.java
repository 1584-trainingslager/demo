import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest{

    @Test
    //@DisplayName("Add two numbers")
    public void testAdd() {
        assertEquals(4, Calculator.add(2, 2));
    }

    @Test
    //@DisplayName("Multiply two numbers")
    public void testMultiply() {
        assertAll(() -> assertEquals(4, Calculator.multiply(2, 2)),
                () -> assertEquals(-4, Calculator.multiply(2, -2)),
                () -> assertEquals(4, Calculator.multiply(-2, -2)),
                () -> assertEquals(0, Calculator.multiply(1, 0)));
    }
}